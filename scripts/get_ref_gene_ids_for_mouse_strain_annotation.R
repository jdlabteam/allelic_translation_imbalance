# Author: Yating Liu
# Description: Query Ensemble database to get ensembl gene ids on reference genome for each transcript in mouse strain (FVB/NJ) annotation (GTF)
#              and then replace the gene ids in attribute column with the reference gene ids and output a new annotation file (GTF)
# Date: 12/18/2018

## Install RMySQL:
### download package from https://cran.r-project.org/src/contrib/RMySQL_0.10.15.tar.gz
### run R CMD INSTALL --configure-args='--with-mysql-dir=/usr/local/mysql \
### --with-mysql-lib=/usr/local/mysql/lib \
### --with-mysql-inc=/usr/local/mysql/include' RMySQL_0.10.15.tar.gz
#install.packages("https://cran.r-project.org/src/contrib/RMySQL_0.10.15.tar.gz", type="source") (not work on mac, but work on cluster)

pkgs <- c("data.table", "stringr", "biomaRt", "DBI", "dplyr")
for (p in pkgs) {
  if (!p %in% installed.packages()) {
    install.packages(p)
  }
}

if (!"RMySQL" %in% installed.packages()) {
  install.packages("https://cran.r-project.org/src/contrib/RMySQL_0.10.15.tar.gz", type="source")
}

library(data.table)
library(stringr)
library(biomaRt)
library(DBI)
library(RMySQL)
library(dplyr)

if (Sys.getenv("debug") == "TRUE") {
  annotationGTF <- "data/sample_FVB_NJ_v1.ensGene.gtf"
  output <- file.path(dirname("data/sample_FVB_NJ_v1.ensGene.gtf"), "sample_FVB_NJ_v1.updated_ids.gtf")
} else {
  args = commandArgs(trailingOnly=TRUE)
  if (length(args) > 1) {
    annotationGTF <- args[1]
    output <- args[2]
    message("read annotation file: ", annotationGTF, "\nOutput new annotation file with reference gene ids to ", output)
  } else {
    stop("Need to pass the path of the annotation file and specify the output file")
  }
}

annGtf <- fread(annotationGTF, data.table = T, sep = "\t", header = F, col.names = c("seqname", "source", "feature", "start", "end", "score", "strand", "frame", "attribute"))

## extract FVBNJ transcript ids. Save both transcript ids with version number (for replace with reference gene ids) and without version number (for query gene ids)
trans <- sapply(str_split(annGtf$attribute, " "), `[[`, 4)
trans_ids <- gsub("\"", "", trans)
trans_ids <- gsub(";", "", trans_ids)
annGtf[, trans_id_with_version := trans_ids]
trans_ids_no_version <- sapply(str_split(trans_ids, "\\."), `[[`, 1)
annGtf[, trans_id := trans_ids_no_version]


## use version 86 because the annotation is based on ensembl gene version 86
if (!file.exists('code/objs/ensembl_mfvbnj_mouse_86.rds')) {
  ensembl = useEnsembl(biomart="ENSEMBL_MART_MOUSE", dataset="mfvbnj_gene_ensembl", version = 86)
  saveRDS(ensembl, 'code/objs/ensembl_mfvbnj_mouse_86.rds')
} else {
  ensembl <- readRDS('code/objs/ensembl_mfvbnj_mouse_86.rds')
}

## get gene ids for the transcripts from FVB mouse strain
trans_info <- getBM(attributes = c("ensembl_gene_id", "ensembl_transcript_id"), filters = "ensembl_transcript_id", values = unique(trans_ids_no_version), mart = ensembl)
query_gene_ids <- unique(trans_info$ensembl_gene_id)
query_gene_ids <- paste0("('",paste0(query_gene_ids, sep="", collapse="','"),"')")

## connect to ensembl server and query the gene ids from reference genome with the gene ids from FVB mouse strain
con <- dbConnect(RMySQL::MySQL(), host = 'ensembldb.ensembl.org', user = 'anonymous', dbname = 'mus_musculus_fvbnj_core_86_1')
ref_gene_ids <- dbGetQuery(con,paste0('select gene.stable_id,gene_attrib.value from gene, gene_attrib where gene.gene_id = gene_attrib.gene_id and gene.stable_id IN ', query_gene_ids, ' and gene_attrib.attrib_type_id = 520;'))

## keep the record for gene ids that are mapped to multiple reference gene ids
dup_gene_ids <- filter(ref_gene_ids, stable_id %in% ref_gene_ids$stable_id[duplicated(ref_gene_ids$stable_id)])
write.csv(dup_gene_ids, file.path(dirname(annotationGTF), "genes_have_multi_ref_ids.csv"), row.names = F, quote = F)

## add reference gene id column to trans_info table
final_trans_info <- left_join(trans_info, ref_gene_ids, by = c("ensembl_gene_id" = "stable_id"))
### remove the version number from the gene ids
final_trans_info$value <- sapply(str_split(final_trans_info$value, "\\."), `[[`, 1)
annGtf2 <- left_join(annGtf, final_trans_info[c("ensembl_transcript_id", "value")], by = c("trans_id" = "ensembl_transcript_id"))
annGtf2 <- as.data.table(annGtf2)
## replace the value of gene_id in attribute column with the gene id from the reference genome
annGtf2[!is.na(value) & str_detect(value, "^ENSMUSG"), attribute := mapply("sub", trans_id_with_version, value, attribute)]
write.table(annGtf2[,-c("trans_id", "trans_id_with_version", "value")], output, sep = '\t', col.names = F, row.names = F, quote = F)

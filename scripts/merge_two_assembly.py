import os
import argparse

'''
This script is used to remove scaffolds sequences, MT, X, and Y chromosomes from the two assemblies. 
Then merge the two assemblies. Rename the chromosomes respectively.
Example: python merge_two_assembly.py ../../data/genome_assembly/Mus_musculus.GRCm38.p4.dna.primary_assembly.fa ../../data/genome_assembly/GCA_001624535.1_FVB_NJ_v1_genomic.fna
'''

def filter_genome_from_ncbi(refAssembly, headerStartStr, strain):
    # the input refAssembly is downloaded from NCBI
    # keep chromosome 1-19
    # remove all scaffolds, X, Y and MT chromosomes
    refAssemblyFiltered = os.path.join(os.path.dirname(refAssembly), os.path.splitext(refAssembly)[0] + "_filtered" + os.path.splitext(refAssembly)[1])
    filtered = open(refAssemblyFiltered, 'w')
    with open(refAssembly, 'r') as ref:
        out = False
        for line in ref:
            if ">" in line:
                if line.startswith(headerStartStr) and "chromosome Y" not in line and "chromosome X" not in line and "mitochondrion" not in line:
                    out = True
                    # rename the header
                    line = ">" + '_'.join([strain, line.split(",")[0].split()[-1]]) + "\n"
                else:
                    out = False
            if (out):
                filtered.write(line)
    filtered.close()
    return refAssemblyFiltered

def filter_genome_from_ensembl(refAssembly, strain):
    # the input refAssembly is downloaded from Ensembl
    # keep chromosome 1-19
    # remove all scaffolds, X, Y and MT chromosomes
    refAssemblyFiltered = os.path.join(os.path.dirname(refAssembly), os.path.splitext(refAssembly)[0] + "_filtered" + os.path.splitext(refAssembly)[1])
    filtered = open(refAssemblyFiltered, 'w')
    with open(refAssembly, 'r') as ref:
        out = False
        for line in ref:
            if ">" in line:
                if "dna:chromosome" in line and ">Y" not in line and ">X" not in line and ">MT" not in line:
                    out = True
                    # rename the header
                    line = ">" + '_'.join([strain, line.split()[0][1:]]) + "\n"
                else:
                    out = False
            if out:
                filtered.write(line)
    filtered.close()
    return refAssemblyFiltered

def merge_two_assembly(refAssemblyFiltered, mouseStrandAssemblyFiltered):
    mergedAssembly = os.path.join(os.path.dirname(mouseStrandAssemblyFiltered), "merged_C57BL6J_FVBNJ_genome_assembly.fa")
    merged = open(mergedAssembly, 'w')
    for file in [refAssemblyFiltered, mouseStrandAssemblyFiltered]:
        with open(file, 'r') as f:
            for line in f:
                merged.write(line)
    merged.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('reference', help = "GRCm38 mouse assembly (Fasta)")
    parser.add_argument('mouse_strand', help = "FVB/NJ mouse strand assembly (Fasta)")
    args = parser.parse_args()
    ref_seq = args.reference
    strand_seq = args.mouse_strand

    # keep chromosome 1-19
    # remove all scaffolds, X and Y chromosome
    ref_seq_filtered = filter_genome_from_ensembl(ref_seq, "C57BL6J")
    mouse_strand_filtered = filter_genome_from_ncbi(strand_seq, ">CM", "FVBNJ")
    merge_two_assembly(ref_seq_filtered, mouse_strand_filtered)
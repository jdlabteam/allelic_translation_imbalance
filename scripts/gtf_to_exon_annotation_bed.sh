#!/bin/bash

# bash script to get exon annotation file with BED format from the merged GTF annotation file, then sort it by chromosome and start position
# BED fields (separate by tab): chrom, chromStart, chromEnd, name, score, strand, gene_id, transcript_id
# chromStart = chromStart -1, because:
## bedtools input bed files format assume BED starts are zero-based and BED ends are one-based
## https://bedtools.readthedocs.io/en/latest/content/overview.html#bed-starts-are-zero-based-and-bed-ends-are-one-based
# sort BED file by specified chromosome order with bedtools sort -faidx
## bedtools intersect need bed file and BAM file in same order. So need to order the chromosomes in bed as the same order as the BAM

# Parameters: [merged genome assembly] [merged genome annotation] [output exon annotation]
# Example: bash gtf_to_exon_annotation_bed.sh

if [ $# -lt 3 ]; then
    echo "You need to input three parameters [merged genome assembly] [merged genome annotation] [output exon annotation]"
    exit
else
    echo "genome assembly = $1"
    echo "genome annotation = $2"
    echo "output exon annotation = $3"
fi

grep ">" $1 | sed 's/>//g' > chrom.order.txt;


ml bedtools;

cat $2 | \
    awk '{OFS = "\t";} {if ($3 == "exon") if($2 != "GCA_001624535.1_FVB_NJ_v1.ensGene.genepred") print $1,$4-1,$5,$3,".",$7,$10,$14,$18; else print $1,$4-1,$5,$3,".",$7,$10,$12,$14; }' | \
    sed 's/"//g; s/;//g' | \
    sort -k1,1V -k2,2n -k3,3n | \
    bedtools sort -faidx chrom.order.txt \
    > $3
# This script is to measure the complexity of each library and output distribution figures 
## Example: Rscript measure_library_complexity.R ../../results/gene_counts_normalized_and_bias.tsv ../../results/figs

library(VGAM)
library(readr)

args <- commandArgs(trailingOnly=TRUE)
if (length(args) < 2) {
  stop("Need to input processed counts, and output figures directory")
} else {
  processed <- args[1]
  ## specify the folder to put library complexity figures
  outputPath  <- args[2]
}

if (!(dir.exists(outputPath))) {
  dir.create(outputPath)
}

print("Importing Data...")
Data<-read_tsv(processed, col_names = T)
colnames(Data)<-c("BC", "Gene", "Lbiase","L","S")
Data$Sbiase<-(1-Data$Lbiase)
Data<-subset(Data,Lbiase != "NA") ## Remove NA samples
print("Complete")
Data$Total<-Data$L+Data$S
Data<-subset(Data, Total>=20) # Require at least 20 reads
Data <- subset(Data, Lbiase > 0 & Lbiase < 1) # attemp to fit betabinomal distribution by removing genes only expressed in B6 or FVB 

## Fitting Function
fitbetabin<-function(N,SampleQuantiles,MaxValue){
  Deviation<-NULL
  Alphas<-NULL
  Beta<-NULL
  Index<-1
  for(i in 1:MaxValue){
    for(z in 1:MaxValue){
      TEMP<-quantile(rbetabinom.ab(N,100,shape1=i,shape2=z)/100,probs=seq(0,1,0.01))
      Deviation[Index]<-as.double(sum(abs((TEMP-SampleQuantiles))))
      Alphas[Index]<-i
      Beta[Index]<-z
      TEMP<-NULL
      Index<-Index+1
    }
  }
  
  #heatmap.2(matrix(data=Deviation,ncol=MaxValue),trace="none",Colv=FALSE,Rowv=FALSE,key.xlab="Deviation")
  fittedAlpha<-Alphas[match(min(as.double(Deviation)),as.double(Deviation))]
  fittedBeta<-Beta[match(min(as.double(Deviation)),as.double(Deviation))]
  dipersionParameter<-1/(1+fittedAlpha+fittedBeta)
  return(c(fittedAlpha,fittedBeta,dipersionParameter))
}
##



for(Bt in as.character(unique(Data$BC))){	
  ## QQ fitting
  N<-length(subset(Data, BC == Bt)$Lbiase)
  quanTest<-quantile(subset(Data, BC == Bt)$Lbiase,probs=seq(0,1,0.01))
  
  Parameters<-fitbetabin(N,quanTest,40)
  
  #bbFit<-quantile(rbetabinom.ab(N,100,shape1=Parameters[1],shape2=Parameters[2])/100,probs=seq(0,1,0.01))
  
  #plot(bbFit,quanTest,xlim=c(0,1),pch=16,cex=0.5,ylab="Data Quantiles",xlab="Fitted Beta-Binom Quantiles")
  #lines(x=c(0,1),y=c(0,1),col="blue")
  
  png(filename=file.path(outputPath, paste("LibComp_",Bt,".png",sep="")), width=500,height=500,type=c("cairo"))
  
  plot(table(round(subset(Data, BC == Bt)$Lbiase,2)),xlim=c(0,1),main=Bt,xlab="Reference Allele Ratio",ylab="Frequency",col="lightblue")
  
  K<-1000000
  
  lines(
    ((table(round(rbetabinom.ab(K,100,shape1=Parameters[1],shape2=Parameters[2])/100,2))/K)*N),
    col="orange",
    type='l'
  )
  
  text(x=0.7,y=round(max(table(round(subset(Data, BC == Bt)$Lbiase,3)))*0.75,0), labels = round(Parameters[3],3))
  dev.off()
}









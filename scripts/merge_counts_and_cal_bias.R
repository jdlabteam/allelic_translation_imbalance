# This script is to merge exon counts of each gene from each barcode (sample) and each mouse strand 
# * output raw gene counts table  
# * output normalize the gene counts by quantiles and expression bias ratio
## Example: Rscript merge_counts_and_cal_bias.R ../jobs/lookup.txt ../../results/exon_counts ../../results
### Note: if you want to use the script for other set of data with different column names, you'll need to set exonCountBEDColnames and remove_str_pattern in the code

options(stringsAsFactors = FALSE)

need_packages <- c("stringr", "data.table")
for (p in need_packages) {
  if (!(p %in% installed.packages())) {
    install.packages(p)
  }
}

library(stringr)
library(data.table)
library(tidyr)
library(dplyr)
library(readr)


args = commandArgs(trailingOnly=TRUE)

if (length(args) < 3) {
  stop("Need to input barcode file, exon counts directory, and output directory")
} else {
  barcode <- args[1]
  countsDir <- args[2]
  outputDir <- args[3]
}

all_counts_info <- data.table()

ID <- scan(barcode, character())

## specify the colnames for exon count (BED) file
exonCountBEDColnames <- c("chr", "start", "end", "type", "score", "strand", "gene", "transcript", "exon_number", "count")

## specify the string pattern to remove from chr to get the genome assembly name
remove_str_pattern <- "_[0-9|X|Y|x|y]+"

bed_files <- list.files(countsDir, pattern = ".bed$", ignore.case = T)
## initialize a table to store upper quantiles of each sample
qtable <- data.frame()
for (i in ID) {
  bed <- bed_files[str_detect(bed_files, i)]
  exonCount <- read_tsv(file.path(countsDir, bed), 
                          col_names = exonCountBEDColnames)
  exonCount <- mutate(exonCount, barcode = i, mouse_strand = str_remove(chr, pattern = remove_str_pattern)) 
  UPQ <- as.numeric(quantile(exonCount$count[exonCount$count>0],probs=c(0.75)))
  qtable <- rbind(qtable, data.frame(barcode = i, upper_quantile = UPQ))
  all_counts_info <- rbind(all_counts_info, exonCount)
}
meanQRT <- sum(qtable$upper_quantile) / length(ID)
## save upper quantile table
write_tsv(qtable, file.path(outputDir, "counts_upper_quantiles.txt"), col_names = T)

counts_table <- spread(all_counts_info, key = mouse_strand, value = count)

## sum up the reads from the same gene and same mouse strain, tidy up the table
message("sum the exon counts from same gene and same sample to get gene counts table")
gene_counts <- counts_table %>% 
  group_by(barcode, gene) %>% 
  summarise(C57BL6J_gene_count = sum(C57BL6J, na.rm = T), FVBNJ_gene_count = sum(FVBNJ, na.rm = T))
## save raw gene counts table for DE
write_tsv(gene_counts, file.path(outputDir, "gene_counts.tsv"), col_names = T)


## normalize counts with mean upper quantile
message("normalize counts with mean upper quantile")
gene_counts_norm <- left_join(gene_counts, qtable, by = "barcode")
stopifnot(!anyNA(gene_counts_norm))
gene_counts_norm <- gene_counts_norm %>%
  mutate(C57BL6J_gene_count_norm = floor(C57BL6J_gene_count / upper_quantile * meanQRT),
         FVBNJ_gene_count_norm = floor(FVBNJ_gene_count / upper_quantile * meanQRT),
         Tot = C57BL6J_gene_count_norm + FVBNJ_gene_count_norm,
         Biase = case_when(Tot > 20 ~ C57BL6J_gene_count_norm/Tot,
                           TRUE ~ NA_real_)) %>% 
  filter(!is.na(Biase)) %>% 
  select(barcode, gene, Biase, C57BL6J_gene_count_norm, FVBNJ_gene_count_norm)
## save the normalized gene counts table for library complexity
write_tsv(gene_counts_norm, file.path(outputDir, "gene_counts_normalized_and_bias.tsv"), col_names = T)

# script to get gene information (chr, ensemlb_gene_id, actual start position) from merged annotation file
library(tidyverse)

args <- commandArgs(trailingOnly=TRUE)
annotation_gtf <- args[1]
outputFile <- args[2]

ann <- read_tsv(annotation_gtf, col_names = c("seqname", "source", "feature", "start", "end", "score", "strand", "frame", "attribute"), comment = "#!") %>% 
  filter(feature == "transcript") %>% 
  mutate(gene = str_remove(sapply(str_split(attribute, pattern = ";"), `[[`, 1), "gene_id ")) %>% 
  transmute(gene = str_remove_all(gene, '"'),
            chr = seqname,
            start = start,
            end = end,
            strand =  strand)

write_tsv(ann, outputFile)
  


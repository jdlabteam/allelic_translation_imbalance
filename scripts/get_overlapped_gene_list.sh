#!/bin/bash

# bash script to get the list of genes that are in both strain annotations

# Parameters: [merged_annotation_gtf] [output gene list file]
# Example: bash get_overlapped_gene_list.sh ../../data/annotation/Mus_musculus_C57BL6J_FVBNJ_merged.gtf ../../data/annotation/mouse_strands_overlapped_genes.tab

if [ $# -lt 2 ]; then
    echo "You need to input two parameters [merged annotation GTF] [output gene list file]"
    exit
else
    echo "merged annotation file = $1"
    echo "output gene list = $2"
fi

grep -v '^#' $1 | awk 'BEGIN {OFS="\t"}; {gsub(/["|;]/, "",$10)}; {print $1,$10}' | uniq > mouse_strands_genes;

grep 'C57BL6J' mouse_strands_genes | awk 'BEGIN {FS = "\t"}; {print $2}' | uniq | sort > C57BL6J_genes;

grep 'FVBNJ' mouse_strands_genes | awk 'BEGIN {FS = "\t"}; {print $2}' | uniq | sort > FVBNJ_genes;

comm -12 C57BL6J_genes FVBNJ_genes > $2;



  
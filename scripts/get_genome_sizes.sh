#!/bin/bash

## The script is to create an chromosome order file via the genome assembly. The chromosome order file will be used for bedtools intersect -g in exon count.

ml samtools;

samtools faidx /scratch/jdlab/yating/Allelic_translation_imbalance/FVB_B6_ATI_analysis/data/genome_assembly/merged_C57BL6J_FVBNJ_genome_assembly.fa;

awk '{OFS = "\t";} {print $1,$2}' /scratch/jdlab/yating/Allelic_translation_imbalance/FVB_B6_ATI_analysis/data/genome_assembly/merged_C57BL6J_FVBNJ_genome_assembly.fa.fai > /scratch/jdlab/yating/Allelic_translation_imbalance/FVB_B6_ATI_analysis/code/jobs/merged_genome_assembly.sizes.genome;

######################################
######################################
#### MEASURING LIBRARY COMPLEXITY ####
######################################
######################################

## here: this script takes in the processed dataset of allele-specific read counts for each gene in each library
# my data is split into three files, one for each sequenced tissue
# input data columns are: chr, genes, cross (F1 reciprocal cross), sex, diet, id (mouse toe ID), barcode (library barcode), lbias (proportion of reads mapping to LG/J chromosome), lcounts (number of reads mapping to LG/J chromosome), scounts (number of reads mapping to SM/J chromosome)
# functions defined here will fit the LG/J-bias proportions (of each library) to a beta-binomial distibution
# a sufficiently complex ASE library has a lower dispersion, or less variability in the data
# two plots are generated:
#   1) a complexity curve for each library, visualizing the L-bias distribution against a beta-binomial distribution
#   2) a bar chart of the rho values for all libraries per tissue, indicating our threshold (> 0.05 = low complexity)

## IMPORTANT changes from previous code versions:
# we know that our allele-specific expression data contains genes with strong expression biases (near 0% LG reads or near 100% LG reads)
# visualizing a distribution of L-biases will reveal "peaks" near 0 and 1
# this is expected, due to the known biology of how this phenomena occurs (cis-regulatory variants, genomic imprinting, etc.)
# these peaks interfere with our library complexity analyses, however, and inflate the dispersion parameters (indicating poor complexity, at first glance)
# our solution: filter out these known & expected peaks (aka very low and very high biases) from the data that is analyzed for complexity
# if the remaining L-biases are sufficiently complex, they will have a low dispersion parameter/rho value
# if the remaining L-biases are not sufficiently complex, they will have a high dispersion parameter/rho value

library("VGAM")
require("ggplot2")
require("cowplot")
require("viridis")
require("tidyverse")
require("reshape2")
theme_set(theme_cowplot())

########################
### DEFINE FUNCTIONS ###
########################

# fitting L-bias quantiles to a beta binomial distribution, estimating dispersion parameters (rho)
fit_beta_binomial <- function(library_quantiles, number_genes, max_value){
  deviation <- NULL
  alpha <- NULL
  beta <- NULL
  iter <- 1
  
  for(i in 1:max_value){
    for(z in 1:max_value){
      temporary <- quantile(rbetabinom.ab(n=number_genes, size=100, shape1=i, shape2=z)/100, probs=seq(0,1,0.01))
      deviation[iter] <- as.double(sum(abs(temporary-library_quantiles)))
      alpha[iter] <- i
      beta[iter] <- z
      iter <- iter+1
    }
  }
  fitted_alpha <- alpha[match(min(as.double(deviation)), as.double(deviation))]
  fitted_beta <- beta[match(min(as.double(deviation)), as.double(deviation))]
  dispersion_parameter <- 1/(1+fitted_alpha+fitted_beta)
  return(c(fitted_alpha, fitted_beta, dispersion_parameter))
}

# plot library complexity curves: L-bias distributions with dispersion parameters
plot_complexity_curve <- function(lbias_quantiles, betabinom_table, dispersion, tissue, barcode){
  # format input data for plotting
  colnames(lbias_quantiles)[2] <- "lbias_freq"
  dataset <- full_join(lbias_quantiles, betabinom_table, by="Var1")
  dataset[is.na(dataset)] <- 0 # convert missing values to 0
  # specify plotting aesthetics
  plot_palette <- viridis_pal(begin=0.35, end=0.75, option="D")(2)
  y_axis_limit <- ceiling(max(dataset$lbias_freq)/100)*100
  # ggplot complexity curves
  to_plot <- ggplot(dataset, aes(x=Var1, y=lbias_freq)) +
    geom_col(color="black", alpha=0.9, position="identity", fill=plot_palette[1]) +
    geom_line(aes(x=Var1, y=Freq), group=1, color=plot_palette[2], size=2) +
    geom_text(x=80, y=y_axis_limit*0.55, hjust=0.5, label=paste("dispersion =", round(dispersion, 3))) +
    scale_y_continuous(expand=c(0.005,0.005), limits=c(0,y_axis_limit)) +
    scale_x_discrete(expand=c(0.015,0.015), breaks=seq(0,1.0,0.2)) +
    labs(x="BL6/(BL6 + FVB) Allelic Bias", y="Frequency", 
         title=paste(toupper(barcode), "- Complexity Curve")) +
    theme(plot.title=element_text(hjust=0.5),
          panel.grid.major=element_line(color="gray90", linetype="solid"),
          panel.grid.minor=element_line(color="gray95", linetype="solid", size=0.5),
          panel.border=element_rect(color="black", size=1.25, linetype="solid"),
          axis.line=element_blank())
  
  plot(to_plot)
  return(to_plot)
}

##################################
### MEASURE LIBRARY COMPLEXITY ###
##################################
setwd("~/Documents/Genetics/Allelic_translation_imbalance/FVB_B6_ATI_analysis/")
gene_counts <- read_tsv("results/gene_counts_normalized_and_bias.tsv")
samples <- read_csv("../FVB_B6_variants/tables/samples.csv")
samples$cell_type <- factor(samples$cell_type, levels = c("PreIP", "IP"), labels = c("RNASeq", "TRAPSeq"))
samples$sample_name <- paste(samples$cell_type, samples$replicate, sep = "_")
gene_counts_tbl <- gene_counts %>% 
  left_join(samples[,c("sample", "sample_name")], by = c("barcode" = "sample")) %>% 
  select(-barcode) %>% 
  rename(lcounts = C57BL6J_gene_count_norm, 
         scounts = FVBNJ_gene_count_norm,
         lbias = Biase,
         barcode = sample_name)
write_tsv(gene_counts_tbl, "results/library_complexity_updated/ProcessedData.txt")


# initialize empty data frame to store dispersion parameters for every tissue x library
dispersion_parameters <- data.frame(tissue=character(), barcode=character(), 
                                    alpha=numeric(), beta=numeric(), dispersion=numeric())

  # read in and filter allele-specific expression data
  processed_data <- read.delim("results/library_complexity_updated/ProcessedData.txt", stringsAsFactors=FALSE) # read in ASE counts for every gene in every library of this tissue
  processed_data$total <- processed_data$lcounts + processed_data$scounts # recalculate total counts
  processed_data <- subset(processed_data, total >= 20) # filter out lowly expressed genes x libraries
  processed_data <- subset(processed_data, lbias != "NA")
  
  # iterate over every library in that tissue's dataset
  for(current_barcode in sort(unique(processed_data$barcode))){
    # subset library-specific ASE data
    tissue <- str_split(current_barcode, "_")[[1]][1]
    cat("\nAnalyzing", tissue, current_barcode, "\n")
    data_subset <- subset(processed_data, barcode == current_barcode)
    data_for_plotting <- data_subset # store full gene-specific dataset for later plotting!
    # calculate L-bias quantiles
    cat(" >> calculating L-bias quantiles (excluding extreme values)\n")
    data_subset <- subset(data_subset, lbias < 0.95 & lbias > 0.05) # exclude extreme peaks from the fitting (exclude expression biases near 0% and 100%)
    lib_quantiles <- quantile(data_subset$lbias, probs=seq(0,1,0.01))
    # fit L-bias quantiles to a beta-binomial distribution, estimate parameters
    cat(" >> fitting beta-binomial distribution\n")
    set.seed(20)
    lib_parameters <- fit_beta_binomial(lib_quantiles, nrow(data_subset), 40)
    # store dispersion parameters for each library
    current_lib <- data.frame(tissue=tissue, barcode=current_barcode, alpha=lib_parameters[1], beta=lib_parameters[2], dispersion=lib_parameters[3])
    dispersion_parameters <- rbind(dispersion_parameters, current_lib)
    # estimate L-bias and beta binomial null distributions
    cat(" >> establishing plotting parameters\n")
    lbias_table <- as.data.frame(table(round(data_for_plotting$lbias, 2)))
    bbfit_table <- as.data.frame(((table(round(rbetabinom.ab(1000000, 100, shape1=lib_parameters[1], shape2=lib_parameters[2])/100, 2))/1000000)*nrow(data_for_plotting)))
    # plot complexity curve & export graph
    cat(" >> plotting complexity curve...\n")
    curve_plot <- plot_complexity_curve(lbias_table, bbfit_table, lib_parameters[3], tissue, current_barcode)
    save_plot(paste0("results/library_complexity_updated/", toupper(current_barcode),"_complexity_curve.png"),
              curve_plot, base_height=4, base_width=6)
    # cleanup
    rm(curve_plot, bbfit_table, lbias_table, current_lib, lib_parameters, lib_quantiles, data_subset)
  }
  rm(current_barcode, processed_data, tissue)




##########################################################
### VISUALIZING DISPERSION PARAMETERS ACROSS LIBRARIES ###
##########################################################

dispersion_parameters$tissue <- as.character(dispersion_parameters$tissue)
dispersion_parameters$barcode <- as.character(dispersion_parameters$barcode)

plot_rho_values <- function(dispersion_data, current_tissue){

  plot_palette <- viridis_pal(begin=0.35, end=0.75, option="D")(2)
  dispersion_data <- subset(dispersion_data, tissue == current_tissue)
  
  to_plot <- ggplot(dispersion_data, mapping=aes(x=barcode, y=dispersion)) +
    geom_hline(yintercept=0.05, size=2, color=plot_palette[2], linetype="dashed") +
    geom_bar(stat="identity", color="black", fill=plot_palette[1]) +
    scale_y_continuous(expand=c(0,0), limits=c(0,0.1), breaks=seq(0,0.1,0.025)) +
    labs(x="Libraries", y="Dispersion (ρ)", title=paste("Library Complexity for", current_tissue)) +
    theme(plot.title=element_text(hjust=0.5),
          axis.text.x=element_text(angle=90, vjust=0.2, hjust=1),
          panel.grid.major=element_line(color="gray90", linetype="solid"),
          panel.grid.minor=element_line(color="gray95", linetype="solid", size=0.5),
          panel.border=element_rect(color="black", size=1.25, linetype="solid"),
          axis.line=element_blank())
  
  print(to_plot)
  return(to_plot)
}

hypo_plot <- plot_rho_values(dispersion_parameters, "RNASeq")  
wat_plot <- plot_rho_values(dispersion_parameters, "TRAPSeq")
save_plot("results/library_complexity_updated/RNASeq_rho_library_complexity.png", hypo_plot, base_height=4, base_width=6)
save_plot("results/library_complexity_updated/TRAPSeq_rho_library_complexity.png", wat_plot, base_height=4, base_width=6)

all_tissues <- plot_grid(hypo_plot, wat_plot, ncol=1, nrow=3)
save_plot("results/library_complexity_updated/all_tissues_rho_library_complexity.png", all_tissues, base_height=12, base_width=6)

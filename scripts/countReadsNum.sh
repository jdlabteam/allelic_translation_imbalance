#!/bin/bash

ID=$( sed -n ${SLURM_ARRAY_TASK_ID}p /scratch/jdlab/yating/Allelic_translation_imbalance/FVB_B6_ATI_analysis/code/jobs/lookup.txt )

projDir=/scratch/jdlab/yating/Allelic_translation_imbalance/FVB_B6_ATI_analysis

printf "Barcode,num_reads_after_filter,num_reads_unique_mapped\n"

for i in $ID; 
	do printf '%s,' $i; 
	printf '%s,' $(zcat "${projDir}/results/post_rrna_filter/${i}_no_rRNA.fq.gz" | echo $((`wc -l`/4)));
	printf '%s\n' $(grep "Uniquely mapped reads number" "${projDir}/results/mapped/${i}_ATI_Log.final.out" | cut -f2 ); 
done
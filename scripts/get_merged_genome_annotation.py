import os
import argparse

'''
This script is used to get a genome annotation (GTF) file to the merged genome assembly. 
Concatenate two genome annotation files into one merged genome annotation file, 
only keep genes located at chromosome 1-19,
and rename the chromosomes according to two genome assemblies.

Example: python get_merged_genome_annotation.py ../../data/annotation/Mus_musculus.GRCm38.86.gtf ../../data/annotation/GCA_001624535.1_FVB_NJ_v1.with_ref_ids.gtf C57BL6J FVBNJ ../../data/annotation/Mus_musculus_C57BL6J_FVBNJ_merged.gtf
'''

def get_merged_annotation(gtfFile_1, gtfFile_2, prefix1, prefix2, mergedGtf):
    outGtf = open(mergedGtf, 'w')
    header = """#!genome-build GRCm38.p4_and_FVBNJ_v1
#!genome-version GRCm38.p4_and_FVBNJ_v1
#!genome-date 2018-12
#!genome-build-accession Mus_musculus.GRCm38.86_and_GCA_001624535.1
#!genebuild-last-updated 2018-12
"""
    outGtf.write(header)
    files = zip([gtfFile_1, gtfFile_2], [prefix1, prefix2])
    for item in files:
        gtf = item[0]
        prefix = item[1]
        with open(gtf, 'r') as f:
            for line in f:
                if line.startswith("#!"):
                    continue
                line = line.split('\t')
                line[0] = line[0].replace("chr", "")
                if line[0] not in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19']:
                    continue
                line[0] = '_'.join([prefix, line[0]])
                line = '\t'.join(line)
                outGtf.write(line)
    outGtf.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('annotation_1', help = "GRCm38 genome annotation file (GTF)")
    parser.add_argument('annotation_2', help = "FVB/NJ genome annotation file (GTF)")
    parser.add_argument('prefix_1', help = "GRCm38 mouse assembly prefix, which is used to rename the chromosomes")
    parser.add_argument('prefix_2', help = "FVB/NJ mouse strand assembly prefix, which is used to rename the chromosomes")
    parser.add_argument('output', help = "specify the path and name of the merged annotation file")
    args = parser.parse_args()
    cbl_annotation = args.annotation_1
    fvb_annotation = args.annotation_2
    prefix_1 = args.prefix_1
    prefix_2 = args.prefix_2
    output = args.output
    # merge the genome annotations and rename the chromosomes
    get_merged_annotation(cbl_annotation, fvb_annotation, prefix_1, prefix_2, output)
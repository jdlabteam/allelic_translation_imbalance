# Allelic Translational Efficiency Imbalance Pipeline (ATEI)

## Genomics data

* VCF files of FVB/NJ-specific variants
    * Based on README from ftp://ftp-mouse.sanger.ac.uk/current_snps/: 
        * This is version 5, unpublished yet; (version 1 has been published)
        * All SNP and indel calls are relative to the reference mouse genome C57BL/6J (GRCm38)
        * Gene models from Ensembl release 78 were used to predict the functional consequences of the SNPs and indels.
        
* Genome assemblies
    * FVB_NJ_v1
        * https://www.ncbi.nlm.nih.gov/assembly/GCA_001624535.1
        * Assembly
            * ftp://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/001/624/535/GCA_001624535.1_FVB_NJ_v1/GCA_001624535.1_FVB_NJ_v1_genomic.fna.gz
            * Do not have Y chromosome
            
    * C57BL/6J (use GRCm38.p4 because the annotation files are from Ensembl version 86)
    
     * Merge two genome assemblies
        * scripts/merge_two_assembly.py
        * This script is used to remove scaffolds sequences, X, Y and MT chromosomes from the two assemblies. (Because FVB/NJ assembly doesn’t have Y chromosome and FVB/NJ annotation doesn’t have Y and MT chromosome)
        * Then merge the two assemblies. Rename the chromosomes respectively.
        * Output: merged_genome_assembly.fa

    ```   
    python merge_two_assembly.py ../../data/genome_assembly/Mus_musculus.GRCm38.dna.primary_assembly.fa ../../data/genome_assembly/GCA_001624535.1_FVB_NJ_v1_genomic.fna
    ```
    
## Annotation data
    
* Genome annotation
    * Mus_musculus annotation for C57BL/6J
        * ftp://ftp.ensembl.org/pub/release-86/gtf/mus_musculus//Mus_musculus.GRCm38.86.gtf.gz
        * Mus_musculus.GRCm38.86.gtf is ensemble version 86, download from Ensembl, to be consistant with FVB_NJ_v1 annotation 
    
    * Mus_musculus annotation for FVB_NJ_v1 annotation
        * http://hgdownload.soe.ucsc.edu/hubs/mouseStrains/hubIndex.html
        * Description: http://hgdownload.soe.ucsc.edu/hubs/mouseStrains/GCA_001624535.1_FVB_NJ_v1/html/GCA_001624535.1_FVB_NJ_v1.ensGene.html
        * bigBed file
            * http://hgdownload.soe.ucsc.edu/hubs/mouseStrains/GCA_001624535.1_FVB_NJ_v1/bbi/GCA_001624535.1_FVB_NJ_v1.ensGene.bb
        * Convert bigBed annotation to GTF using UCSC utilities
        
        ```
        bigBedToBed GCA_001624535.1_FVB_NJ_v1.ensGene.bb GCA_001624535.1_FVB_NJ_v1.ensGene.bed
        bedToGenePred GCA_001624535.1_FVB_NJ_v1.ensGene.bed GCA_001624535.1_FVB_NJ_v1.ensGene.genepred
        genePredToGtf file GCA_001624535.1_FVB_NJ_v1.ensGene.genepred GCA_001624535.1_FVB_NJ_v1.ensGene.gtf
        ```
        
        * Replace the gene ids in attribute column with the reference gene ids and output a new annotation file (GTF)
            * Script: get_ref_gene_ids_for_mouse_strain_annotation.R
            
            ```
            sbatch get_ref_ids_for_annotation.sbatch
            ```
    
    * Mus_musculus.GRCm38.86_merged.gtf
        * code/scripts/get_merged_genome_annotation.py
        * This script is used to merged the annotations, remove gene annotations not on chromosome 1-19, and rename the chromosomes according to merged genome assembly
        
        ```
        python get_merged_genome_annotation.py ../../data/annotation/Mus_musculus.GRCm38.86.gtf ../../data/annotation/GCA_001624535.1_FVB_NJ_v1.with_ref_ids.gtf C57BL6J FVBNJ ../../data/annotation/Mus_musculus_C57BL6J_FVBNJ_merged.gtf
        ```
        
* Filter merged annotation, only genes in both mouse strands
    * Get overlapped gene list mouse_strands_overlapped_genes.tab
    
    ```
    sbatch code/scripts/get_overlapped_gene_list.sh data/annotation/Mus_musculus_C57BL6J_FVBNJ_merged.gtf data/annotation/mouse_strands_overlapped_genes.tab
    ```
    
    * Filter the merge annotation file and only keep the overlapped genes
    
    ```
    grep -Ff data/annotation/mouse_strands_overlapped_genes.tab data/annotation/Mus_musculus_C57BL6J_FVBNJ_merged.gtf > data/annotation/Mus_musculus_C57BL6J_FVBNJ_merged_overlapped.gtf 
    ```
    
* Get exon annotation BED file
    * gtf_to_exon_annotation_bed.sh
    
    ```
    bash gtf_to_exon_annotation_bed.sh ../../data/genome_assembly/merged_C57BL6J_FVBNJ_genome_assembly.fa ../../data/annotation/Mus_musculus_C57BL6J_FVBNJ_merged_overlapped.gtf ../../data/annotation/Mus_musculus_C57BL6J_FVBNJ_merged_exon_overlapped.bed
    ```
   
    * Check if the BED file is valid
    
    ```
    bedops --ec -u ../../data/annotation/Mus_musculus_C57BL6J_FVBNJ_merged_exon_overlapped.bed
    ```
    
## RNA-Seq analysis
* Reads after trimming and filtering rrna
    * Use the results *.fq.gz from /scratch/jdlab/yating/FVB_B6_variants/results/post_rrna_filter

* Build Star index for merged genome

* Align with star
    * Specific changed parameters
    
    ```
    ## only keep the mapped reads
    --outSAMunmapped None
    ## max number of multiple alignments allowed for a read:
    --outFilterMultimapNmax 1 
    ## output alignments as sorted BAM
    --outSAMtype BAM SortedByCoordinate
    ```

* The samples read stats generated by scripts/countReadsNum.sh is at results/samples_reads_stat.csv

* Count how many aligned reads fall into gene exons
    * Trouble shooting bedtools intersect
        * Unless you use the -sorted option, bedtools currently does not support chromosomes larger than 512Mb
        * BED annotation file chromosome starts should be zero-based and chromosome ends should be one-based
        * BED annotation file chromosomes should be ordered the same as the BAM file
    * Generate a genome file the defines the expected chromosome order in the input files for use with the -sorted option
   
    ```
    bash get_genome_sizes.sh
    ```
    
    * Count reads
    
    ```
    sbatch count_reads.sbatch
    ```
    
* Merge exon counts to get gene countsCount reads: output raw counts that are used for differential expression analysis
    
    ```
    sbatch merge_counts_cal_bias.sbatch
    ```
    
    * Output:
        * Merge exon counts and output raw gene counts for DE 
            * results/gene_counts.tsv
        * Calculate upper quantile counts for each sample, normalize the gene counts by upper quantiles, and calculate bias. Output normalized counts and bias score for QC step: library complexity
            * results/gene_counts_normalized_and_bias.tsv

* Measure complexity of each library
	
	```
	RScript measure_library_complexity_new.R ../../results/gene_counts_normalized_and_bias.tsv ../../results/figs
	```
	
## MiRNA analysis
    * see branch "mirna"
    
## Standard RNA-Seq analysis
    * see branch "standard_rnaseq"




